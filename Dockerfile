
FROM registry.centos.org/centos/centos:7

MAINTAINER Karanbir Singh <container-feedback.inb@karan.org>

ENV GOGS_VER=0.11.29

LABEL License=GPLv2
LABEL Version=${GOGS_VER}

RUN useradd -ms /bin/bash -d /srv/git gogs
COPY ./root /
RUN yum -y install --setopt=tsflags=nodocs git && \
    yum -y upgrade && yum clean all && \
    curl -L -o /tmp/gogs.tar.gz https://dl.gogs.io/${GOGS_VER}/linux_amd64.tar.gz && \
    cd /opt && tar zxvf /tmp/gogs.tar.gz && chmod +x gogs/gogs && \
    chmod 777 /srv/git && chmod 777 /opt/git/.ssh/


EXPOSE 3000

USER 1001

ENTRYPOINT ["/usr/local/bin/rungogs" ]
